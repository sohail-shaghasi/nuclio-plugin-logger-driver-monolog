<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\logger\driver\monolog
{
	use nuclio\core\ClassManager;
	use nuclio\plugin\logger\psr3\LoggerInterface;
	use Monolog\Logger;
   	use Monolog\Handler\StreamHandler;
   	use nuclio\plugin\provider\manager\Manager as ProviderManager;
   	
	<<provides('logger::monolog')>>
	class Monolog implements LoggerInterface
	{
		/**
		 * @var Monolog\Logger|null
		 */
		private Logger $logger;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Monolog
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(Map<string,string> $options=Map{})
		{
			$this->logger = new Logger($options->get('channel')['logger']);
			$path = $options->get('channel')['path'];
			
			$this->logger->pushHandler(new StreamHandler($path, Logger::INFO));	


		}
		
		/**
		 * @inheritdoc
		 */
		public function emergency(string $message, array<mixed> $context=[]):self
		{
			$this->logger->addEmergency($message, $context);
			return this;
		}
		
		/**
		 * @inheritdoc
		 */
		public function alert(string $message, array<mixed> $context=[]):self
		{
			$this->logger->addAlert($message, $context);
			return this;
		}
		
		/**
		 * @inheritdoc
		 */
		public function critical(string $message, array<mixed> $context=[]):self
		{
			$this->logger->addCritical($message, $parameter);
			return this;
		}
		
 		/**
		 * @inheritdoc
		 */
		public function error(string $message, array<mixed> $context=[]):self
		{
			$this->logger->addError($message, $context);
			return this;
		}
		
		/**
		 * @inheritdoc
		 */
		public function warning(string $message, array<mixed> $context=[]):self
		{
			$this->logger->addWarning($message, $context);
			return this;
		}
		
		/**
		 * @inheritdoc
		 */
		public function notice(string $message, array<mixed> $context=[]):self
		{
			$this->logger->addNotice($message, $context);
			return this;
		}
		
		/**
		 * @inheritdoc
		 */
		public function info(string $message, array<mixed> $context=[]):self
		{
			$this->logger->addInfo($message, $context);
			return this;
		}
		
		/**
		 * @inheritdoc
		 */
		public function debug(string $message, array<mixed> $context=[]):null
		{
			$this->logger->addDebug($message, $context);
			return null;
		}
		
		/**
		 * @inheritdoc
		 */
		public function log(int $level, $message, array<mixed> $context=[]):null
		{
			$this->logger->addLog($message, $context);
			return this;
		}
	}
}
